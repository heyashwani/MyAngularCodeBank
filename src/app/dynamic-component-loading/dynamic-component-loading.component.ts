import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { DynamicComponentLoading1Component } from '../dynamic-component-loading1/dynamic-component-loading1.component';
import { Service1Service } from '../services/Service1/service1.service';

@Component({
  selector: 'app-dynamic-component-loading',
  templateUrl: './dynamic-component-loading.component.html',
  styleUrls: ['./dynamic-component-loading.component.css']
})
export class DynamicComponentLoadingComponent implements OnInit {

  @ViewChild("formTemplate",{read:ViewContainerRef}) formRef:ViewContainerRef;
  eleRef: HTMLElement;
  componentRef: any = {};

  constructor(private userService:Service1Service,private comp:ComponentFactoryResolver) { 
    this.userService.HeaderDisplay.emit(true);
  }

  ngOnInit() {
  }

  loadLogin(){
    this.formRef.clear();
    const loginComp = this.comp.resolveComponentFactory(DynamicComponentLoading1Component)
    const componentRef = this.formRef.createComponent(loginComp)
    this.componentRef = componentRef
    componentRef.instance.childEvent.subscribe((event: string) => {
      // this.service.addFilter();
      alert(event)
    });

    
  }
  global:any = {
    id:1,
    name:"kumar",
    status:true
  }
  myClick(){
    ++this.global.id
    this.componentRef.instance.childValue = this.global
  }

}
