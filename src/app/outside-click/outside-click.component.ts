import { Component, HostListener, OnInit } from '@angular/core';
import { Service1Service } from '../services/Service1/service1.service';

@Component({
  selector: 'app-outside-click',
  templateUrl: './outside-click.component.html',
  styleUrls: ['./outside-click.component.css']
})
export class OutsideClickComponent implements OnInit {

  clickResponse:boolean;

  constructor(private service1:Service1Service) {
    this.service1.HeaderDisplay.emit(true);
  }

  ngOnInit() {
  }

  @HostListener('document:click', ['$event'])
    onDocumentClick(evt){
      let elem = document.getElementById("head")
      this.clickResponse = this.service1.detectClick(elem,evt)
      console.log("service called",this.clickResponse)
        
    }

}
