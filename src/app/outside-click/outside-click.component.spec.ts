import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsideClickComponent } from './outside-click.component';

describe('OutsideClickComponent', () => {
  let component: OutsideClickComponent;
  let fixture: ComponentFixture<OutsideClickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideClickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideClickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
