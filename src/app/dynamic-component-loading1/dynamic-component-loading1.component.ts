import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dynamic-component-loading1',
  templateUrl: './dynamic-component-loading1.component.html',
  styleUrls: ['./dynamic-component-loading1.component.css']
})
export class DynamicComponentLoading1Component implements OnInit {

  @Output() childEvent = new EventEmitter()
  @Input() childValue:any;
  
  constructor() { }

  ngOnInit() {
  }

  onSubmit(){
    this.childEvent.emit("ashwani")
  }

}
