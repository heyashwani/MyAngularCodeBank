import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicComponentLoading1Component } from './dynamic-component-loading1.component';

describe('DynamicComponentLoading1Component', () => {
  let component: DynamicComponentLoading1Component;
  let fixture: ComponentFixture<DynamicComponentLoading1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicComponentLoading1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicComponentLoading1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
