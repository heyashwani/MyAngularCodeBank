import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { urlValidator } from '../custom-validators/custom.validators';
import { passwordValidator } from '../custom-validators/password';
import { Service1Service } from '../services/Service1/service1.service';



@Component({
  selector: 'app-custom-validator',
  templateUrl: './custom-validator.component.html',
  styleUrls: ['./custom-validator.component.css']
})
export class CustomValidatorComponent implements OnInit {

  
  loginForm:FormGroup
  

  constructor(private _formBuilder:FormBuilder,private service1:Service1Service) { 
    this.service1.HeaderDisplay.emit(true);
  }

  ngAfterViewInit(): void {
    
    
  }

  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email:["",[Validators.required,urlValidator]],
      password:["",[Validators.required,passwordValidator]]
    })
  }

  onFocus() {
    document.getElementById("message").style.display = "block";
  }

  onBlur() {
    document.getElementById("message").style.display = "none";
  }

  onKeyUp(){
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length");
    // Validate lowercase letters
    var lowerCaseLetters = /[a-z]/g;
    if(this.loginForm.get("password").value.match(lowerCaseLetters)) {  
      
      letter.classList.remove("invalid");
      letter.classList.add("valid");
    } else {
      letter.classList.remove("valid");
      letter.classList.add("invalid");
    }
  
    // Validate capital letters
    var upperCaseLetters = /[A-Z]/g;
    if(this.loginForm.get("password").value.match(upperCaseLetters)) {  
      capital.classList.remove("invalid");
      capital.classList.add("valid");
    } else {
      capital.classList.remove("valid");
      capital.classList.add("invalid");
    }

    // Validate numbers
    var numbers = /[0-9]/g;
    if(this.loginForm.get("password").value.match(numbers)) {  
      number.classList.remove("invalid");
      number.classList.add("valid");
    } else {
      number.classList.remove("valid");
      number.classList.add("invalid");
    }
  
    // Validate length
    if(this.loginForm.get("password").value.length >= 8) {
      length.classList.remove("invalid");
      length.classList.add("valid");
    } else {
      length.classList.remove("valid");
      length.classList.add("invalid");
    }
  }

  onSubmit(){
    console.log(this.loginForm)
  }

  onClickedOutside(e: Event) {
    console.log('Clicked outside:', e);
  }

}
