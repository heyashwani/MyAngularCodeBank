import { AbstractControl } from '@angular/forms';
export function passwordValidator(control: AbstractControl) {
	var lowerCaseLetters = /[a-z]/g;
  if (!control.value.match(lowerCaseLetters)) {
    return { passwordValid: true };
  }

	var upperCaseLetters = /[A-Z]/g;
	if(!control.value.match(upperCaseLetters)){
		return { passwordValid: true };
	}

	var numbers = /[0-9]/g;
	if(!control.value.match(numbers)){
		return { passwordValid: true };
	}

	if(control.value.length < 8){
		return { passwordValid: true };
	}
  return null;
}